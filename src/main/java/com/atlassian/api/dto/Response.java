package com.atlassian.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    @JsonProperty("status")
    private Boolean status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("payload")
    private Object payload;

    public Boolean getStatus() {
        return status;
    }

    public Response setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Response setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getPayload() {
        return payload;
    }

    public Response setPayload(Object payload) {
        this.payload = payload;
        return this;
    }
}
