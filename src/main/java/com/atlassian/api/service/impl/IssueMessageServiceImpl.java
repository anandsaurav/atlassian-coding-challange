package com.atlassian.api.service.impl;

import com.atlassian.api.service.IssueMessageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class IssueMessageServiceImpl implements IssueMessageService {

    @Value("${queue_name}")
    private String queueName;

    @Autowired
    private JmsTemplate defaultJmsTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    private org.slf4j.Logger logger = LoggerFactory.getLogger(IssueMessageServiceImpl.class);

    @Override
    public void sendMessage(Object data) {
        try {
            String message = objectMapper.writeValueAsString(data);
            defaultJmsTemplate.convertAndSend(queueName, message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

    }
}
