package com.atlassian.api.exception;

import com.atlassian.api.dto.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class APIExceptionHandler {

    @ExceptionHandler(APIException.class)
    public ResponseEntity processAPIException(Exception ex) {
        return new ResponseEntity<>(new Response().setMessage(ex.getMessage()).setStatus(false), HttpStatus.valueOf(500));
    }

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity processInputException(Exception ex) {
        return new ResponseEntity<>(new Response().setMessage(ex.getMessage()).setStatus(false), HttpStatus.valueOf(400));
    }
}
