# Running the Spring Boot Application
  -> Clone the repository :- git clone https://anandsaurav@bitbucket.org/anandsaurav/atlassian-coding-challange.git

  -> Run :- mvn clean spring-boot:run

# Changing the configuration
  The application is built to run on ElasticMQ which does not require any authentication. To run the application on production using Amazon sqs, we need to change the properties in 'application.properties' file for the properties :- access_key, secret_key, region

# API response
  There was no requirement for any response format, so I have come up with a response structure, which will be standard for all the response that we will get from any api that will be created.
