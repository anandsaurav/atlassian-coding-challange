package com.atlassian.api.service.impl;

import com.atlassian.api.dto.Response;
import com.atlassian.api.exception.APIException;
import com.atlassian.api.exception.InvalidInputException;
import com.atlassian.api.model.Issue;
import com.atlassian.api.model.QueueIssueMsg;
import com.atlassian.api.service.IssueMessageService;
import com.atlassian.api.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class IssueServiceImpl implements IssueService {

    @Value("${jira_base_url}")
    private String jiraBaseUrl;

    @Value("${jira_issue_url_endpoint}")
    private String jiraIssueEndpoint;

    @Autowired
    private IssueMessageService messageService;

    @Override
    public Response computeStoryPoints(String query, String descriptiveName) throws APIException {
        try {
            String completeIssueUrl = jiraBaseUrl + String.format(jiraIssueEndpoint, query);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<List<Issue>> response = restTemplate.exchange(
                    completeIssueUrl,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Issue>>(){});
            List<Issue> issues = response.getBody();
            if (!CollectionUtils.isEmpty(issues)) {
                int sum = issues.stream().mapToInt(x -> x.getFields().getStoryPoints()).sum();
                QueueIssueMsg msg = new QueueIssueMsg();
                msg.setName(descriptiveName).setTotalPoints(sum);
                messageService.sendMessage(msg);
            } else {
                throw new InvalidInputException("No issue found using the query.");
            }
            return new Response().setStatus(true).setMessage("success");
        } catch (Exception e) {
            throw new APIException(e.toString());
        }

    }
}
