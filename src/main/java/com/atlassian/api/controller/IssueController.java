package com.atlassian.api.controller;

import com.atlassian.api.dto.Response;
import com.atlassian.api.exception.APIException;
import com.atlassian.api.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/issue")
public class IssueController {

    @Autowired
    private IssueService issueService;

    @RequestMapping(method = RequestMethod.GET, value = "/sum", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> processSumOfStoryPoints(@RequestParam(value = "query") String query,
            @RequestParam(value = "name") String descriptiveName) throws APIException {
            Response apiResponse= issueService.computeStoryPoints(query, descriptiveName);
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
