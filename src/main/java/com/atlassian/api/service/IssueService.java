package com.atlassian.api.service;

import com.atlassian.api.dto.Response;
import com.atlassian.api.exception.APIException;
import org.springframework.stereotype.Component;

@Component
public interface IssueService {
    Response computeStoryPoints(String query, String descriptiveName) throws APIException;
}
