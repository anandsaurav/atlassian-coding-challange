package com.atlassian.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QueueIssueMsg {

    @JsonProperty("name")
    private String name;

    @JsonProperty("totalPoints")
    private int totalPoints;

    public String getName() {
        return name;
    }

    public QueueIssueMsg setName(String name) {
        this.name = name;
        return this;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public QueueIssueMsg setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
        return this;
    }
}
