package com.atlassian.api.service;

public interface IssueMessageService {
    void sendMessage(Object data);
}
